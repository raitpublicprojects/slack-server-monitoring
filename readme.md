# Server monitoring script

Simple server monitorig script sending alerts to Slack app in case CPU, memory usage is too high and/or disk space is getting full.
Test output to Slack can be seen on Test.png (PS! alert variables are set really low on image for testing purpose).

## how to use

- Create slack app from scratch: https://api.slack.com/apps?new_app=1
- choose "Incoming Webhooks"
- Activate Incoming Webhooks -> Add New Webhook to Workspace
- add slack channel where to ntotify
- Install app to your workspace 
- copy-paste webhook as variable in script. ex.:
-- webhook=https://hooks.slack.com/services/XXX/XXX/XXX
- you can change variables by editing their value:
-- `cpuloadalert=90`
    CPU load percentage when alert is sent. Default 90%
-- `memalert=95`
    Memory percentage when alert is sent. Default 95%
-- `disks_to_monitor="/dev/sda2 /dev/sda3"`
    disks to monitor. write as bash list. Default is "/dev/sda2" and "/dev/sda3"
-- `disk_alert_percentage=75`
    Percentage when alert is sent if the drives are getting full
- Add script to crontab
--https://www.digitalocean.com/community/tutorials/how-to-use-cron-to-automate-tasks-ubuntu-1804
    
## packages needed

install extra packages needed to run script. (Tested only on Ubuntu 21.10 )

- mpstat
-- for centos 7 `$ yum install sysstat`
--for ubuntu `$ apt-get install sysstat`
- AWK
-- for centos 7 `$ yum install gawk`
--for ubuntu `$ apt-get install gawk`

list of all package used (ubuntu) (iproute2, mpstat, awk)

