#!/bin/bash

webhook=https://hooks.slack.com/services/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
cpuloadalert=0
memalert=0
disks_to_monitor="/dev/sda2"
disk_alert_percentage=2



# No need to change after this line
message=""
date='date +%m/%d/%Y'
time='date +%k:%m:%s'
ip=$(ip addr show | grep 'inet '| grep -v 127.0.0.1 | awk '{print $2}' | cut -d/ -f1 | head -n 1)

testcpu() {
    cpuload=$((100-$(mpstat | grep -Po 'all.* \K[^ ]+$' | cut -d $(locale decimal_point) -f 1)))
    if  [[ $cpuload -gt $3 ]];
        then
                text="server: $1 - has high cpu load: $cpuload ";
                curl -X POST -H Content-type: application/json --data '{"text": "'"$text"'" }' $2
    fi
}

testmem() {
    mempercent=$(("100 * $(free -m | awk 'NR==3{print $3}')/$(free -m | awk 'NR==2{print $2}')"))
    if  [[ $mempercent -gt $3 ]];
        then
                text="server: $1 - is using a lot of memory: $mempercent ";
                curl -X POST -H Content-type: application/json --data '{"text": "'"$text"'" }' $2
    fi
}

testdisk() {
    for disk in $3 ; do
    diskpercentage=$(df -H | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5 " " $1 }' | grep "/dev/sda2" | awk 'NR==1{print $1}' | tr -d '%')
        if  [[ $diskpercentage -gt $disk_alert_percentage ]];
            then
                    text="server: $1 - disk ($disk) usage is high: $diskpercentage ";
                    curl -X POST -H Content-type: application/json --data '{"text": "'"$text"'" }' $2
        fi
    done
}

testcpu "$ip" "$webhook" "$cpuloadalert"
testmem "$ip" "$webhook" "$memalert"
testdisk "$ip" "$webhook" "$disks_to_monitor"
